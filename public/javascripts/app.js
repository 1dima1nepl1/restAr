
//TODO: убрать весь функционал на сервер

var log = document.getElementById('login');
log.addEventListener('click', () => {
    login()
});

function login() {
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;

    console.log(email, password);

    firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
        console.log(error);
    });

    setTimeout(() => {
        var user = firebase.auth().currentUser;

        if (user !== null) {
            window.location = "/main";
        } else {
            window.alert("try again");
        }
    }, 1500)

}

function logout() {
    firebase.auth().signOut();
    if (firebase.auth().currentUser === null) {
        window.sessionStorage.removeItem('user');
        setTimeout(() => {
            window.location = "/";
        }, 1000)
    }

}

function main() {
    window.location = '/main';
}

function changeUserName() {
    let user = firebase.auth().currentUser;

    const userName = document.getElementById("newName_field").value;

    user.updateProfile({
        displayName: userName,
    }).then(function () {
        window.alert('Update successful.');
    }).catch(function (error) {
        window.alert(error);
    });

    var db = firebase.database();
    db.ref('users/' + user.uid + '/user_name/').set(userName);

    console.log(user.displayName);

}


function changeUserSurname() {
    let user = firebase.auth().currentUser;
    const newSurname_field = document.getElementById("newSurname_field").value;

    var db = firebase.database();
    db.ref('users/' + user.uid + '/user_surname/').set(newSurname_field).then(function () {
        window.alert('Update successful.');
    });
}

function changeUserPhone() {
    let user = firebase.auth().currentUser;
    const newPhone_field = document.getElementById("newPhone_field").value;

    var db = firebase.database();
    db.ref('users/' + user.uid + '/user_phone/').set(newPhone_field).then(function () {
        window.alert('Update successful.');
    });
}

function changeUserCountry() {
    let user = firebase.auth().currentUser;
    const newCountry_field = document.getElementById("newCountry_field").value;

    var db = firebase.database();
    db.ref('users/' + user.uid + '/user_country/').set(newCountry_field).then(function () {
        window.alert('Update successful.');
    });
}

function changeUserCity() {
    let user = firebase.auth().currentUser;
    const newCity_field = document.getElementById("newCity_field").value;

    var db = firebase.database();
    db.ref('users/' + user.uid + '/user_city/').set(newCity_field).then(function () {
        window.alert('Update successful.');
    });

}

function changeUserAvatar() {
    let user = firebase.auth().currentUser;

    const userAvatar = document.getElementById("newAvatar_field").value;

    console.log(userAvatar);

    user.updateProfile({
        photoURL: userAvatar,
    }).then(function () {
        window.alert('Update successful.');
    }).catch(function (error) {
        window.alert(error);
    });

    console.log(user.photoURL);
}

function getDataCMS() {
    var data = firebase.database().ref('/models/');

    var time = 500;

    data.on('value', function (snapshot) {
        var DATA = snapshot.val();
        //  console.log( DATA)
        for (var prop in DATA) {
            var model = DATA[prop];

            for (var propin in model) {
                // console.log(propin);

                if (propin === "img") {
                    const imgUrl = model[propin].model_imgUrl;

                    console.log(model[propin].model_imgUrl);
                    model.src = '/images/model.obj';

                    var list = document.getElementById('photos');
                    var li = document.createElement('li');
                    var div = document.createElement('div');
                    var img = document.createElement('img');
                    var p = document.createElement('p');
                    div.className = "col-md-4";
                    img.src = imgUrl;
                    img.className = "photo";
                }
                if(propin === "model_name"){
                    p.innerHTML +="<b>Model name:</b> "+model[propin]+"<br>"
                }
                if(propin === "model_author"){
                    p.innerHTML +="<b>Author:</b> "+model[propin]+"<br>"
                }
                if(propin === "model_category"){
                    p.innerHTML +="<b>Category:</b> "+model[propin]+"<br>"
                }
                if(propin === "model_description"){
                    p.innerHTML +="<b>Description:</b> "+model[propin]+"<br>"
                }
                if(propin === "model_price"){
                    p.innerHTML +="<b>Price:</b> "+model[propin]+"<br>"
                }
            }
            div.appendChild(img);
            div.appendChild(p);
            li.appendChild(div);
            list.insertBefore(div, list.firstChild);
        }
    });
}
