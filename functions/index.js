var createError = require('http-errors');
const functions = require('firebase-functions');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require("body-parser");

var app = express();

app.use(express.json());

//app.use(bodyParser());


var authRouter = require('./routes/auth.js')(app);
var postsRouter = require('./routes/posts.js')(app);
var renderRouter = require('./routes/render.js')(app);


// view engine setup

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



app.get('/' , (req, res) => {
  res.redirect('/checkCMS');
});



// catch 404 and forward to error handler
/*app.use(function(req, res, next) {
  next(createError(404));
});*/

// error handler
/*app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error.ejs');
});*/
exports.app = functions.https.onRequest(app);
//module.exports = app;
