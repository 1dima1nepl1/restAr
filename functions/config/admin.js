var firebase = require('firebase-admin');

var serviceAccount = require('../sprint-3-key.json');

module.exports = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    apiKey: "AIzaSyCWBwA4nQKHOBwdVXVRnDAxIMB_Mw5ycgY",
    authDomain: "sprint-3-48247.firebaseapp.com",
    databaseURL: "https://sprint-3-48247.firebaseio.com",
    projectId: "sprint-3-48247",
    storageBucket: "sprint-3-48247.appspot.com",
    messagingSenderId: "205485785816"
});