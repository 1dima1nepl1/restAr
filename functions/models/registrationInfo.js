var firebase = require('../config/config.js');

var database = firebase.database();

exports.writeUserData = (userId, name, surname, phone, email, country, city) => {
    database.ref('users/' + userId).set({
        user_name: name,
        user_surname: surname,
        user_phone: phone,
        user_email: email,
        user_country: country,
        user_city: city
    });

};