var firebase = require('../config/config.js');

var database = firebase.database();
//const storageService = firebase.storage();
//const storageRef = storageService.ref();


exports.writeModelData = (name, price, author, category, description, img, mObj, dObj, dText) => {

    const imgRef = storageRef.child(`models/${name}/img/`).put(img); //create a child directory called images, and place the file inside this directory

    imgRef.on('state_changed', (snapshot) => {
    }, (error) => {
        console.log(error);
    }, () => {
        var imgUrl = storageService.ref(`images/${name}/img/`).getDownloadURL();
    });

    const mObjRef = storageRef.child(`models/${name}/mobObj/`).put(mObj); //create a child directory called images, and place the file inside this directory

    mObjRef.on('state_changed', (snapshot) => {
    }, (error) => {
        console.log(error);
    }, () => {
        var mobObj = storageService.ref(`images/${name}/mobObj/`).getDownloadURL();
    });

    const dObjRef = storageRef.child(`models/${name}/desObj/`).put(dObj); //create a child directory called images, and place the file inside this directory

    dObjRef.on('state_changed', (snapshot) => {
    }, (error) => {
        console.log(error);
    }, () => {
        var desObj = storageService.ref(`images/${name}/desObj/`).getDownloadURL();
    });

    const dTextRef = storageRef.child(`models/${name}/desText/`).put(dText); //create a child directory called images, and place the file inside this directory

    dTextRef.on('state_changed', (snapshot) => {
    }, (error) => {
        console.log(error);
    }, () => {
        var desText = storageService.ref(`images/${name}/desText/`).getDownloadURL();
    });

    database.ref('models/' + name).set({
        model_name: name,
        model_price: price,
        model_author: author,
        model_category: category,
        model_description: description,
        model_imgUrl: imgUrl,
        model_mobObj: mobObj,
        model_desObj: desObj,
        model_desText: desText
    });

    res.send("end")
};