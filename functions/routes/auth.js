var authController = require('../controllers/authController.js');


module.exports = function (app) {
    app.post('/login', authController.login);

    app.post('/loginCMS', authController.loginCMS);

    app.post('/registration', authController.registration);

    app.post('/registrationCMS', authController.registrationCMS);

    app.get('/check', authController.check);

    app.get('/checkCMS', authController.checkCMS);

    app.get('/logOut', authController.logOut);

    app.get('/logOutCMS', authController.logOutCMS);

    app.post('/resetPass', authController.resetPass);

    app.post('/resetPassCMS', authController.resetPassCMS);

};
