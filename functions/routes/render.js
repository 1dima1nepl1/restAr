var renderController = require('../controllers/renderController.js');

//TODO: страница товара
//TODO: webVR для каталога и страници товара

module.exports = function (app) {
    app.get('/login', renderController.loginRender);

    app.get('/main', renderController.mainRender);

    app.get('/registration', renderController.registrationRender);

    app.get('/adminSettings', renderController.adminSettings);

    app.get('/resetPassCMS', renderController.resetPassCMS);

    app.get('/checkYourEmail', renderController.checkYourEmail);

    app.get('/profileSettings', renderController.profileSettings);

    app.get('/error', renderController.error);

    app.get('/newRestaurant', renderController.newRestaurant);

    app.get('/newDish', renderController.newDish);


};
