var postsController = require('../controllers/postsController.js');

module.exports = function(app){
    app.get('/modelsData', postsController.verifyToken, postsController.modelsData);

    app.get('/modelsDataCMS', postsController.modelsDataCMS);

    app.get('/restaurantsData', postsController.restaurantsData);

    app.post('/restaurantData', postsController.restaurantData);

    app.get('/dishesData', postsController.dishesData);

    app.post('/dishData', postsController.dishData);

    app.post('/dishCategory', postsController.dishCategory);

    app.post('/dishPrice', postsController.dishPrice);

    app.get('/restaurantsId', postsController.restaurantsId);

};

