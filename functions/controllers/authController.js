var firebase = require('../config/config.js');
var admin = require('../config/admin.js');
const functions = require('firebase-functions');
var registrationInfo = require('../models/registrationInfo.js');
var jwt = require('jsonwebtoken');
const validator = require('validator');

let code = 'securityKey';

//TODO: вынести валидацию в клиента

//TODO: реализовать аутентификацию для CMS через API

exports.login = (req, res) => {
    let appCode = req.query.appCode;
    let user_email = req.body.email;
    let user_password = req.body.password;

    if (validator.isEmpty(user_email) && validator.isEmpty(user_password)) {
        res.json({
            "message": "Field email and passwords are empty"
        });
    } else if (validator.isEmpty(user_email)) {
        res.json({
            "message": "Field email is empty"
        });
    } else if (validator.isEmpty(user_password)) {
        res.json({
            "message": "Field password is empty"
        });
    } else if (!validator.isEmail(user_email)) {
        res.json({
            "message": "Field email is incorect"
        });
    }

    if (appCode !== null && appCode === code) {
        firebase.auth().signInWithEmailAndPassword(user_email, user_password).then(function(user){
            if (user !== null) {
                jwt.sign({user}, 'secretkey', (err, token) => {
                    if (err) {
                        res.sendStatus(403);
                    }
                    res.json({
                        message: "Logged in",
                        token
                    });
                });
            }
            else {
                res.json({
                    "message": "user not created"
                });
            }
            return this;
        }).catch(function (error) {
            res.send(error);
        });

       /* setTimeout(function () {
            var user = firebase.auth().currentUser;

            if (user !== null) {
                jwt.sign({user}, 'secretkey', (err, token) => {
                    if (err) {
                        res.sendStatus(403);
                    }
                    res.json({
                        message: "Logged in",
                        token
                    });
                });
            }
            else {
                res.json({
                    "message": "user not created"
                });
            }
        }, 1000)*/



    } else {
        res.json({
            "message": "invalid security app key"
        });
    }
};

exports.loginCMS = (req, res) => {
    console.log(firebase.auth);

    let user_email = req.body.email;
    let user_password = req.body.password;

    firebase.auth().signInWithEmailAndPassword(user_email, user_password).catch(function (error) {
        res.redirect('/')
    });
    setTimeout(function () {
        var user = firebase.auth().currentUser;

        if (user !== null) {
            res.redirect('/main')
        }
        else {
            res.redirect('/')
        }
    }, 1000);
};

exports.registration = (req, res) => {
    let appCode = req.query.appCode;

    let email = req.body.email;
    let password = req.body.password;
    let name = req.body.name;
    let surname = req.body.surname;
    let phone = req.body.phone;
    let country = req.body.country;
    let city = req.body.city;

    if (!validator.isEmail(email)) {
        res.json({
            "message": "Field email is incorect"
        });
    }

    let fields = [name, surname, phone, email, password, country, city];

    var fieldEmpty = "This fields are empty:";
    var empty = false;
    for (var i = 0; i < fields.length; i++) {
        if (validator.isEmpty(fields[i])) {
            fieldEmpty += (i + " ");
            empty = true;
        }
    }
    if (empty) {
        res.json({
            "message": fieldEmpty
        })
    }

    if (appCode !== null && appCode === code && empty === false) {
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
            res.send(error)
        });

        setTimeout(function () {
            let user = firebase.auth().currentUser;

            if (user !== null) {
                let user_Id = user.uid;

                registrationInfo.writeUserData(user_Id, name, surname, phone, email, country, city);

                res.json({
                    message: "user created"
                });

            } else {
                res.json({
                    message: "user not created"
                });
            }
        }, 1500);
    } else {
        res.json({
            message: "invalid security app key"
        });
    }

};
exports.registrationCMS = (req, res) => {

    let user_email = req.body.email;
    let user_password = req.body.password;


    firebase.auth().createUserWithEmailAndPassword(user_email, user_password).catch(function (error) {
        res.redirect('/registration')
    });
    setTimeout(function () {
        let user = firebase.auth().currentUser;

        if (user !== null) {
            let user_Id = user.uid;
            let user_name = req.body.name;
            let user_surname = req.body.surname;
            let user_phone = req.body.phone;
            let user_country = req.body.country;
            let user_city = req.body.city;

            registrationInfo.writeUserData(user_Id, user_name, user_surname, user_phone, user_email, user_country, user_city);

            res.redirect('/login')
        } else {
            res.redirect('/registration')
        }
    }, 1500);
};

exports.check = (req, res) => {
    let appCode = req.query.appCode;
    let user = firebase.auth().currentUser;

    if (appCode !== null && appCode === code) {
        if (user !== null) {
            res.json({
                message: "user already logged in"
            });
        } else {
            res.json({
                message: "please sign in"
            });
        }
    } else {
        res.json({
            message: "invalid security app key"
        });
    }
};

exports.checkCMS = (req, res) => {
    let user = firebase.auth().currentUser;

    if (user !== null) {
        res.redirect('/main');
    } else {
        res.redirect('/login');
    }
};


exports.logOut = (req, res) => {
    let appCode = req.query.appCode;

    if (appCode !== null && appCode === code) {
        firebase.auth().signOut().then(() => {
            res.json({
                message: "you logged out successfully"
            });
            return this;
        }).catch(function (error) {
            res.send(error);
        });
    } else {
        res.json({
            message: "invalid security app key"
        });
    }
};

exports.logOutCMS = (req, res) => {

    firebase.auth().signOut().then(() => {
        res.redirect('/');
        return this;
    }).catch(function (error) {
        res.send(error);
    });
};


exports.resetPass = (req, res) => {
    let appCode = req.query.appCode;
    let user_email = req.body.email;

    if (validator.isEmpty(user_email)) {
        res.json({
            "message": "Field email is empty"
        });
    } else if (!validator.isEmail(user_email)) {
        res.json({
            "message": "Field email is incorect"
        });
    }

    var data = firebase.database().ref('/users/');

    data.on('value', function (snapshot) {
        var DATA = snapshot.val();
        //  console.log( DATA)
        for (var prop in DATA) {
            var model = DATA[prop];

            for (var propin in model) {
                if(propin === "user_email"){
                    if(model[propin] === user_email){
                        if (appCode !== null && appCode === code) {
                            firebase.auth().sendPasswordResetEmail(user_email).catch(function (error) {
                                res.send(error);
                            });
                            res.json({
                                message: "check your email"
                            });
                        }
                        else {
                            res.json({
                                message: "invalid security app key"
                            });
                        }
                    }
                }
            }
        }
    });

    res.json({
        message: "User with this email not exists"
    })

};

exports.resetPassCMS = (req, res) => {
    let user_email = req.body.email;

    firebase.auth().sendPasswordResetEmail(user_email).catch(function (error) {
        res.send(error);
    });
    res.redirect('/checkYourEmail');
};