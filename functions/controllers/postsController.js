var firebase = require('../config/config.js');
var registrationInfo = require('../models/registrationInfo.js');
var jwt = require('jsonwebtoken');
var formidable = require('formidable');
var newObjUpload = require('../models/newObjUpload.js');


let code = 'securityKey';

exports.modelsData = (req, res) => {
    let appCode = req.query.appCode;

    if (appCode !== null && appCode === code) {
        //TODO: реализовать токены через firebase
        jwt.verify(req.token, 'secretkey', (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {

                var data = firebase.database().ref('/models/');
                data.on('value', function (snapshot) {
                    res.send(snapshot.val());
                });
            }
        });
    }
};

//TODO: реализовать получение списка моделей для CMS в функции modelsData

exports.modelsDataCMS = (req, res) => {

    var data = firebase.database().ref('/models/');
    data.on('value', function (snapshot) {
        res.send(snapshot.val());
    });
};

exports.verifyToken = (req, res, next) => {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        return next();
    } else {
        // Forbidden
        res.sendStatus(403);
    }

};

exports.restaurantsData = (req, res) => {
    var data = firebase.database().ref('/restaurants/');
    data.on('value', function (snapshot) {
        res.send(snapshot.val());
    });
};

exports.restaurantData = (req, res) => {
    let restaurant_id = req.body.id;

    if (restaurant_id === "") {
        res.json({
            "error": "no parametrs were sent"
        })
    } else {
        var rest = firebase.database().ref('/restaurants/' + restaurant_id);

        var restaurant_name = "";

        rest.on('value', function (snapshot) {
            var temp = snapshot.val();

            if (temp === null) {
                res.json({
                    "error": "nothing was found"
                })
            } else {
                restaurant_name = temp.restaurant_name;
            }
        });

        var dishesData = firebase.database().ref('/dishes/');

        var restDishes = {};


        setTimeout(() => {
            console.log(restaurant_name);

            dishesData.on('value', function (snapshot) {
                var DATA = snapshot.val();

                for (var prop in DATA) {
                    var model = DATA[prop];

                    for (var propin in model) {
                        if (propin === "restaurant_name") {
                            if (model[propin] === restaurant_name) {
                                var imgArr = [];
                                imgArr.push(model.img.dish_img);
                                model.img = imgArr;

                                var modelArr = [];
                                modelArr.push(model.ar.dish_ar);
                                model.ar = modelArr;

                                restDishes[model.dish_name] = model;
                            }
                        }
                    }
                }

                rest.on('value', function (snapshot) {
                    var data = snapshot.val();
                    var imgArr = [];
                    co(data.img.restaurant_img);
                    co(data);

                    imgArr.push(data.img.restaurant_img);
                    data.img = imgArr ;
                    data.restaurant_dishes = restDishes;
                    res.send(data);
                });
            });


        }, 1000)
        function co(temp) {
            console.log(temp);
        }
    }
};

exports.restaurantsId = (req, res) => {
    var data = firebase.database().ref('/restaurants/');
    var restId = [];

    data.on('value', function (snapshot) {
        var DATA = snapshot.val();
        var iter = 0;

        for (var prop in DATA) {

            restId.push(prop);
            co(prop);
        }
    });

    function co(temp) {
        console.log(temp);
    }

    var result = {
        "restaurantsId": restId
    }

    setTimeout(() => {
        res.send(result);
    }, 1000);

};

exports.dishesData = (req, res) => {
    var data = firebase.database().ref('/dishes/');
    data.on('value', function (snapshot) {
        res.send(snapshot.val());
    });
};

exports.dishData = (req, res) => {
    let dish_id = req.body.id;

    if (dish_id === "") {
        res.json({
            "error": "no parametrs were sent"
        })
    } else {
        var data = firebase.database().ref('/dishes/' + dish_id);

        data.on('value', function (snapshot) {
            var temp = snapshot.val();

            if (temp === null) {
                res.json({
                    "error": "nothing was found"
                })
            } else {
                res.send(temp);
            }
        });
    }
};


exports.dishCategory = (req, res) => {
    let dish_category = req.body.name;

    var dishesData = firebase.database().ref('/dishes/');

    var categoryDishes = {};

    dishesData.on('value', function (snapshot) {
        var DATA = snapshot.val();

        for (var prop in DATA) {
            var model = DATA[prop];

            for (var propin in model) {
                if (propin === "dish_category") {
                    if (model[propin] === dish_category) {
                        categoryDishes[model.dish_name] = model;
                    }
                }
            }
        }
        res.send(categoryDishes);
    });
};

exports.dishPrice = (req, res) => {
    let price_from = Number(req.body.from);
    let price_to = Number(req.body.to);

    if (price_from === 0 && price_to === 0) {
        res.json({
            "error": "no parametrs were sent"
        })
    }

    console.log(price_from, price_to);

    var dishesData = firebase.database().ref('/dishes/');

    var priceDishes = {};

    dishesData.on('value', function (snapshot) {
        var DATA = snapshot.val();

        for (var prop in DATA) {
            var model = DATA[prop];

            for (var propin in model) {
                if (propin === "dish_price") {

                    var price = Number(model[propin]);

                    if (price_from !== 0 && price_to !== 0) {
                        if (price >= price_from && price <= price_to) {
                            priceDishes[model.dish_name] = model;
                        }
                    }
                    if (price_from !== 0 && price_to === 0) {
                        if (price >= price_from) {
                            priceDishes[model.dish_name] = model;
                        }
                    }
                    if (price_from === 0 && price_to !== 0) {
                        if (price <= price_to) {
                            priceDishes[model.dish_name] = model;
                        }
                    }
                }
            }
        }
        res.send(priceDishes);
    });
};
