exports.loginRender = (req, res) => {
    res.render('login.ejs');
};

exports.registrationRender = (req, res) => {
    res.render('registration.ejs');
};

exports.mainRender = (req, res) => {
    res.render('main.ejs');
};

exports.adminSettings = (req, res) => {
    res.render('adminSettings.ejs');
};

exports.resetPassCMS = (req, res) => {
    res.render('resetPassCMS.ejs');
};

exports.checkYourEmail = (req, res) => {
    res.render('checkYourEmail.ejs');
};
exports.error = (req, res) => {
    res.render('error.ejs');
};
exports.profileSettings = (req, res) => {
    res.render('profileSettings.ejs');
};
exports.newRestaurant = (req, res) => {
    res.render('newRestaurant.ejs');
};
exports.newDish = (req, res) => {
    res.render('newDish.ejs');
};



